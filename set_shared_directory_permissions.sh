#!/usr/bin/env sh

     DIR="/home/stuff"
   GROUP="stuff"
 DIRPERM="2770"
FILEPERM="g+rw"

# SGID
# https://www.redhat.com/sysadmin/suid-sgid-sticky-bit

# For other readable directory use: 2775
# Sometimes FILEPER="660" is better
# For other readable files use:
# g+rw,o+r or 664

# Optional: Make manifest of existing files before
# changing them. This can be useful to fix a bad permission change
# ls -RAlk ${DIR} > manifest-${DIR}-$(date +%F).txt
# determine file count from this manifest:
# cat manifest-${DIR}-$(date +%F).txt | wc -l

chgrp -R ${GROUP} ${DIR}

chmod ${DIRPERM} ${DIR}

# print0 and xargs -0 handle files with spaces in the name

find ${DIR} -type d -print0 | xargs -0 chmod ${DIRPERM}

find ${DIR} -type f -print0 | xargs -0 chmod ${FILEPERM}



