#!/usr/bin/env sh

  DIR="/home/stuff"
GROUP="stuff"

# Make backup of file setting before changes
#ls -RAlk ${DIR} > manifest.txt
#find ${DIR} -ls > manifest.txt

# SGID
# https://www.redhat.com/sysadmin/suid-sgid-sticky-bit


chgrp -R ${GROUP} ${DIR}

# for group shared directory that others won't have access to

chmod 2770 ${DIR}

find ${DIR} -type d -print0 | xargs -0 chmod 2770

find ${DIR} -type f -print0 | xargs -0 chmod g+rw

# for group shared directory that others with have read-only access to:

chmod 2775 ${DIR}

find ${DIR} -type d -print0 | xargs -0 chmod 2775

find ${DIR} -type f -print0 | xargs -0 chmod g+rw,o+r


