
# create class directory and account

useradd -c "This is a shared directory for a class" class101
chmod 2770 ~class101

# create instructor directory and account

useradd -c "Instructor shared directory for class101 group" -d /home/class/class101_i class101_i
chmod 2700 ~class101_i

